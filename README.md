# Battery-Notifier

This is a bash script that notifies you if your battery charge reaches certain thresholds... I don't know what else to type here... 

## Full Charge:<br>

![alt text](https://gitlab.com/Jiei_Tuna/WM-Battery-Notifier/-/raw/c8739d18e72dedc7806a3b1d3a53d764bd44b3e8/FullCharge.png) 

## Low Charge:<br>

![alt text](https://gitlab.com/Jiei_Tuna/WM-Battery-Notifier/-/raw/c8739d18e72dedc7806a3b1d3a53d764bd44b3e8/LowCharge.png) 

## REQUIREMENTS

<ul>
  <li>
    dunst ( notification daemon ) </li>
  
  <li>
    acpi ( to get info bout yer battery ) </li>
  
  <li>
    sox ( OPTIONAL: this can play mp3 files ) </li>
</ul>
